include:
  - template: "Workflows/MergeRequest-Pipelines.gitlab-ci.yml"

variables:
  CACHE_DIR: ${CI_PROJECT_DIR}/.cache
  NAMESPACE: ${CI_PROJECT_NAMESPACE}
  PROJECT: ${CI_PROJECT_NAME}
  REGISTRY: ${CI_REGISTRY}
  REGISTRY_PASSWORD: ${CI_REGISTRY_PASSWORD}
  REGISTRY_USER: ${CI_REGISTRY_USER}
  COMMIT_SHA: ${CI_COMMIT_SHORT_SHA}
  GIT_TAG: ${CI_COMMIT_TAG}
  GIT_BRANCH: ${CI_COMMIT_BRANCH}
  GIT_DEFAULT_BRANCH: ${CI_DEFAULT_BRANCH}

stages:
  - build_image
  - prepare_image
  - dependencies
  - build
  - deploy
  - test

.references:
  buildah:
    tag: &buildah_tag
      - buildah login --tls-verify=${TLS_VERIFY-true} -u "${REGISTRY_USER}" -p "${REGISTRY_PASSWORD}" ${REGISTRY}
      - buildah pull --tls-verify="${TLS_VERIFY-true}" "${REGISTRY}/${NAMESPACE}/${PROJECT}/buildah:${ORIGINAL_TAG}"
      - buildah tag "${REGISTRY}/${NAMESPACE}/${PROJECT}/buildah:${ORIGINAL_TAG}" "${REGISTRY}/${NAMESPACE}/${PROJECT}/buildah:${NEW_TAG}"
      - buildah push --tls-verify="${TLS_VERIFY-true}" --format v2s2 "${REGISTRY}/${NAMESPACE}/${PROJECT}/buildah:${NEW_TAG}"
    changes: &buildah_changes
      - .gitlab-ci.yml
      - Makefile
      - build/*
      - buildah/*

# Build the image if there are changes.
buildah_build:
  stage: build_image
  script:
    - env
  variables:
    IMAGE_TAG: ${CI_COMMIT_SHORT_SHA}
  cache:
    key: dnf
    paths:
      - ${CACHE_DIR}
    when: always
  interruptible: true
  rules:
    - changes: *buildah_changes

# Tag the new build as latest if there are changes and this is the default
# branch.
buildah_tag_latest:
  stage: prepare_image
  script:
    - env
  interruptible: true
  needs:
    - buildah_build
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes: *buildah_changes
      variables:
        NEW_TAG: latest
        ORIGINAL_TAG: ${CI_COMMIT_SHORT_SHA}

# Tag the new build with the Git tag if there are changes and this is a tag
# build.
buildah_tag_tag:
  stage: prepare_image
  script:
    - env
  interruptible: true
  needs:
    - buildah_build
  rules:
    - if: $CI_COMMIT_TAG
      variables:
        NEW_TAG: ${CI_COMMIT_TAG}
        ORIGINAL_TAG: ${CI_COMMIT_SHORT_SHA}

# Tag the new build with the Git branch if there are changes and this is a
# branch build.
buildah_tag_branch:
  stage: prepare_image
  script:
    - env
  interruptible: true
  needs:
    - buildah_build
  rules:
    - if: $CI_COMMIT_TAG
      variables:
        NEW_TAG: ${CI_COMMIT_TAG}
        ORIGINAL_TAG: ${CI_COMMIT_SHORT_SHA}

# Tag the latest build with the Git commit if there are no changes.
buildah_tag_commit:
  stage: prepare_image
  script:
    - env
  interruptible: true
  rules:
    - changes: *buildah_changes
      when: never
    - when: on_success
      variables:
        NEW_TAG: ${CI_COMMIT_SHORT_SHA}
        ORIGINAL_TAG: latest

npm_install:
  stage: dependencies
  script:
    - env
  cache:
    key: node
    paths:
      - ${CACHE_DIR}
      - node_modules
    when: always
  interruptible: true
  needs:
    - job: buildah_build
      optional: true
    - job: buildah_tag_commit
      optional: true
  rules:
    - if: $CI_MERGE_REQUEST_IID == null
      when: never
    - changes:
        - package-lock.json
      variables:
        NEEDS_REINSTALL: "true"
    - when: on_success

build:
  stage: build
  script:
    - env
  cache:
    key: dnf
    paths:
      - ${CACHE_DIR}
    when: always
  interruptible: true
  needs:
    - job: buildah_build
      optional: true
    - job: buildah_tag_commit
      optional: true
  rules:
    - changes:
        - .gitlab-ci.yml
        - Makefile
        - build/*
        - database/*
        - mediawiki/*
        - proxy/*
        - redis/*
        - reverse-proxy/*
    - changes: *buildah_changes

review:
  stage: deploy
  script:
    - env
  environment:
    name: review/${CI_COMMIT_REF_NAME}
    url: https://${CI_ENVIRONMENT_SLUG}-mediawiki-mediawiki-${KUBE_NAMESPACE}.${KUBE_INGRESS_BASE_DOMAIN}
    deployment_tier: development
    on_stop: finish_review
  interruptible: true
  needs:
    - job: build
      optional: true
  rules:
    - if: $CI_MERGE_REQUEST_IID == null
      when: never
    - when: on_success

finish_review:
  stage: deploy
  script:
    - env
  environment:
    name: review/${CI_COMMIT_REF_NAME}
    action: stop
  allow_failure: true
  variables:
    GIT_STRATEGY: none
  needs:
    - job: build
      optional: true
  rules:
    - if: $CI_MERGE_REQUEST_IID == null
      when: never
    - when: manual

test:
  stage: test
  script:
    - env
  environment:
    name: review/${CI_COMMIT_REF_NAME}
    action: prepare
  cache:
    key: node
    paths:
      - ${CACHE_DIR}
      - node_modules
    policy: pull
  artifacts:
    name: test-results-$CI_PIPELINE_IID
    expose_as: test-results
    paths:
      - test-results/
    reports:
      junit: results.xml
    when: on_failure
    public: false
  needs:
    - job: npm_install
      optional: true
    - review
  rules:
    - if: $CI_MERGE_REQUEST_IID == null
      when: never
    - when: on_success
